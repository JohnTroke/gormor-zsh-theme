#!/bin/bash

function changeFile() {
	sed -i  -e "$1s/.*/$2/g" .gormor-config
}

printf "Welcome to the color config creator for the gormor zsh theme!\nTo start press [ENTER] :\n"
read SELECTION
clear
printf "Enter 'default' to set values to default, or 'new' to make a new color scheme.\nWrite your choice then press [ENTER] :\n"
read SELECTION
if [[ $SELECTION = 'default' ]]; then
	changeFile 1 cyan
	changeFile 2 blue
	changeFile 3 red
	changeFile 4 ❖
	changeFile 5 ϟ
	changeFile 6 %T
elif [[ $SELECTION = 'new' ]]; then
	clear
	printf "Enter the time format you prefer\nWrite your choice then press [ENTER] :\n"
	echo
	echo "Write one of [24,12]"
	read SELECTION

	if [[ $SELECTION = 24 ]]; then
		changeFile 6 %T
	elif [[ $SELECTION = 12 ]]; then
		changeFile 6 %t
	fi

	clear
	printf "Enter the prompt's user@host font color.\nWrite your choice then press [ENTER] :\n"
	echo
	echo "Write one of [black,red,green,yellow,blue,magenta,cyan,white]"
	read FG

	changeFile 1 $FG

	clear
	printf "Enter the prompt's directory font color.\nWrite your choice then press [ENTER] :\n"
	echo
	echo "Write one of [black,red,green,yellow,blue,magenta,cyan,white]"
	read FG

	changeFile 2 $FG

	clear
	printf "Enter the prompt's first newline font color.\nWrite your choice then press [ENTER] :\n"
	echo
	echo "Write two of [black,red,green,yellow,blue,magenta,cyan,white]"
	read FG

	changeFile 3 $FG

	clear
	printf "Enter the prompt's user first symbol.\nWrite your choice then press [ENTER] :\n"
	read FG

	changeFile 4 $FG

	clear
	printf "Enter the prompt's root first symbol.\nWrite your choice then press [ENTER] :\n"
	read FG

	changeFile 5 $FG

fi

clear
echo "Close and re-open your zshell to see changes"
echo