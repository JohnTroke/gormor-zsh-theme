#!/bin/bash

exec 6< `dirname $0`/.gormor-config

read line1 <&6
read line2 <&6
read line3 <&6
read line4 <&6
read line5 <&6
read line6 <&6

exec 6<&-

USER_FG_COLOR=$line1
DIRECTORY_FG_COLOR=$line2
FIRSTLINE_FG_COLOR=$line3
INITIAL_SYMBOL=$line4
ROOT_SYMBOL=$line5
TIME_FORMAT=$line6
TERMINAL_LENGTH=30

function getUser() {

	if [[ $UID = 0 ]]; then
		STRING="%F{$FIRSTLINE_FG_COLOR%}$ROOT_SYMBOL "
	else
		STRING="%F{$FIRSTLINE_FG_COLOR%}$INITIAL_SYMBOL "
	fi
	STRING+=" %F{$USER_FG_COLOR%}$USER at %m%F{$FIRSTLINE_FG_COLOR%}$reset_color"
	echo $STRING
}
function getDir() {
	FINAL='%c'

	COUNT=`echo $PWD`
	COUNT=${COUNT#$HOME}
	COUNT=`echo $COUNT | wc -m | sed 's/ //g'`
	COUNT=`expr $COUNT - 1`
	if [[ "$COUNT" -gt "$TERMINAL_LENGTH" ]]; then
		STRING2+="/.../"
		STRING2+="$FINAL"
	else
		STRING2=%~
	fi
	echo "%F{$DIRECTORY_FG_COLOR%}$STRING2$reset_color"
	STRING2=''
}	

function getGit() {
  	local ref dirty mode repo_path
 	repo_path=$(git rev-parse --git-dir 2>/dev/null)
	if $(git rev-parse --is-inside-work-tree >/dev/null 2>&1); then
	    dirty=$(parse_git_dirty)

	    ref="$(git symbolic-ref HEAD 2> /dev/null)" 

	   	if [[ -n $dirty ]]; then
	      echo -n "on %F{yellow%}"
	    else
	      echo -n "on %F{green%}"
	      ref+=" ✔  "
	      ref+="$(git rev-parse --short HEAD 2> /dev/null)"
	    fi

	    
	
	    if [[ -e "${repo_path}/BISECT_LOG" ]]; then
	      mode=" <B>"
	    elif [[ -e "${repo_path}/MERGE_HEAD" ]]; then
	      mode=" >M<"
	    elif [[ -e "${repo_path}/rebase" || -e "${repo_path}/rebase-apply" || -e "${repo_path}/rebase-merge" || -e "${repo_path}/../.dotest" ]]; then
	      mode=" >R>"
	    fi

	    setopt promptsubst
	    autoload -Uz vcs_info

	    zstyle ':vcs_info:*' enable git
	    zstyle ':vcs_info:*' get-revision true
	    zstyle ':vcs_info:*' check-for-changes true	
	   	zstyle ':vcs_info:*' unstagedstr '✎  '
	    zstyle ':vcs_info:*' stagedstr '⊕'
	    zstyle ':vcs_info:*' formats '%u%c'
	    zstyle ':vcs_info:*' actionformats ' %u%c'

	    vcs_info
	    if [[ MODIFIED -eq "0" ]]; then
	    	MODIFIED=''
	    else
	    	MODIFIED+="M "
	    fi
	    if [[ ADDED -eq "0" ]]; then
	    	ADDED=''
	    else
	    	ADDED+="??"
	    fi
	    if [[ STAGED -eq "0" ]]; then
	    	STAGED=''
	    else
	    	STAGED+="A "
	    fi
	    echo -n "[${ref/refs\/heads\//}] ${vcs_info_msg_0_%%}${mode}"
	fi
}

function status() {
	RETVAL=$?
	[[ $RETVAL -ne 0 ]] && symbols+="%{%F{red}%}✕  $RETVAL "
	symbols+="$TIME_FORMAT"

	[[ -n "$symbols" ]] && echo "$symbols %{%f%}"
}

PROMPT='$(getUser) in $(getDir) $(getGit)
%F{$FIRSTLINE_FG_COLOR%}→  %{%f%}'
RPROMPT='$(status)'